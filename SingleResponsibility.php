<?php

namespace Acme\Reporting;

use Auth, DB, Exception;
use Acme\Repositories\SalesRepository;
use Acme\Reporting\SalesOutputInterface;

class SalesReporter
{
    private $repo;

    public function __construct(SalesRepository $repo)
    {
        $this->repo = $repo;
    }

    public function between($startDate, $endDate, SalesOutputInterface $formatter)
    {
        // Application logic, should be extracted into a controller.
        // if (! Auth::check()) {
        //     throw new Exception('Authentication required for reporting.');
        // }

        $sales = $this->repo->between($startDate, $endDate);

        return $formatter->output($sales);
    }

    // Persistence logic, extracted into Acme\Repositories\SalesRepository
    // protected function queryDBForSalesBetween($startDate, $endDate)
    // {
    //     return DB::table('sales')->whereBetween('created_at', [$startDate, $endDate])->sum('charge') / 100;
    // }

    // Refactor Options
    //      1. Leave the formatting to the consumer of this class.
    //      2. Extract into an output contract. (Acme\Reporting\SalesOutputInterface)
    // protected function format($sales)
    // {
    //     return "<h1>Sales: $sales</h1>";
    // }
}

?>

<?php

namespace Acme\Repositories;

use DB;

class SalesRepository
{
    protected function between($startDate, $endDate)
    {
        return DB::table('sales')->whereBetween('created_at', [$startDate, $endDate])->sum('charge') / 100;
    }
}

?>

<?php

namespace Acme\Reporting;

interface SalesOutputInterface
{
      public function output($sales);
}

?>

<?php

namespace Acme\Reporting;

class HTMLOutput extends Acme\Reporting\SalesOutputInterface
{
    public function output($sales)
    {
        return "<h1>Sales: $sales</h1>";
    }
}

?>