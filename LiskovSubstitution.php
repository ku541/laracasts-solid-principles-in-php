<!-- Adherence List
    1. Signature must match.
    2. Preconditions can't be greater.
    3. Post conditions at least equal to.
    4. Exception types must match. -->

<?php

class VideoPlayer
{
    public function play($file)
    {
        
    }
}

class AVIVideoPlayer extends VideoPlayer
{
    public function play($file)
    {
        // Preconditions for the subclass can not be greater. 
        // if (pathinfo($file, PATHINFO_EXTENSION) !== 'avi') {
        //     throw new Exception;
        // }
    }
}

?>

<?php

interface LessonRepositoryInterface
{
    /**
     * Fetch all records.
     *
     * @return array 
     */
    public function getAll();
}

class FileLessonRepository implements LessonRepositoryInterface
{
    function getAll()
    {
        return [];
    }
}

class DBLessonRepository implements LessonRepositoryInterface
{
    function getAll()
    {
        // Return values can not be different.
        // return Lesson::all();
        return Lesson::all()->toArray();
    }
}

?>