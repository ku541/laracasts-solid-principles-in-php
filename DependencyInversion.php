<?php

class PasswordReminder
{
    private $dbConnection;

    // High level modules like PasswordReminder should not depend upon low level
    // modules like MySQLConnection.
    // public function __construct(MySQLConnection $dbConnection)
    // {
    //     $this->dbConnection = $dbConnection;
    // }

    public function __construct(ConnectionInterface $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }
}

interface ConnectionInterface
{
    public function connect();
}

class DBConnection implements ConnectionInterface
{
    public function connect()
    {
        
    }
}

?>