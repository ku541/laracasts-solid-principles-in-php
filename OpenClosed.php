<?php

namespace Acme;

class Square implements Shape
{
    public $width;
    public $height;

    public function __construct($height, $width)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function area()
    {
        return $this->width * $this->height;
    }
}

?>

<?php

namespace Acme;

class AreaCalculator
{
    public function calculate(array $shapes)
    {
        foreach ($shapes as $shape) {
            // Separate extensible behavior behind an interface, and flip the dependencies.
            // if (is_a($shape, 'Square')) {
            //     $area[] = $shape->width * $shape->height;
            // } else {
            //     $area[] = pi() * $shape->radius * $shape->radius;
            // }
            $area = $shape->area();
        }

        return array_sum($area);
    }
}

?>

<?php

namespace Acme;

class Circle implements Shape
{
    public $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function area()
    {
        return pi() * $this->radius * $this->radius;
    }
}

?>

<?php

namespace Acme;

interface Shape
{
    public function area();
}

?>