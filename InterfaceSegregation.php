<?php

interface ManageableInterface
{
    public function beManaged();
}

interface WorkableInterface
{
    public function work();
}

interface SleepableInterface
{
    public function sleep();
}

class Captain
{
    public function manage(WorkableInterface $worker)
    {
        $worker->beManaged();
    }
}

class HumanWorker implements WorkableInterface, ManageableInterface, SleepableInterface
{
    public function work()
    {
        return 'Human Working';
    }

    public function sleep()
    {
        return 'Human Sleeping';
    }
    
    function beManaged()
    {
        $this->work();
        $this->sleep();
    }
}

class AndroidWorker implements WorkableInterface, ManageableInterface
{
    public function work()
    {
        return 'Android Working';
    }

    function beManaged()
    {
        $this->work();
    }

    // Clients should not be forced to implement interfaces they do not use.
    // public function sleep()
    // {
    //     return null;
    // }
}

?>